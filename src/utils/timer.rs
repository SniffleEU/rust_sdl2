use std::time::{Duration, Instant};

pub struct Timer {
    is_started: bool,
    is_paused: bool,
    started_ticks: Instant,
    paused_ticks: Instant,
}

impl Timer {
    pub fn new() -> Timer {
        Timer {
            is_started: false,
            is_paused: false,
            started_ticks: Instant::now(),
            paused_ticks: Instant::now(),
        }
    }

    pub fn restart(&mut self) {
        self.is_started = true;
        self.is_paused = false;

        self.started_ticks = Instant::now();
        // self.paused_ticks = 0;
    }

    pub fn stop(&mut self) {
        //self.started_ticks = 0;
        // self.started_ticks = 0;
        // self.paused_ticks = 0;
        self.is_started = false;
        self.is_paused = false;
    }

    pub fn pause(&mut self) {
        if self.is_started && !self.is_paused {
            self.is_paused = true;
            self.paused_ticks = Instant::now();
            //self.started_ticks = 0;
        }
    }

    pub fn unpause(&mut self) {
        if self.is_started && self.is_paused {
            self.is_paused = false;
            self.started_ticks = std::time::Instant::now();
            //self.paused_ticks = 0;
        }
    }

    ///Returns time since timer started
    pub fn get_ticks(&self) -> Duration {
        if self.is_started {
            if self.is_paused {
                self.paused_ticks.elapsed()
            } else {
                self.started_ticks.elapsed()
            }
        } else {
            Duration::new(0, 0)
        }
    }
}
