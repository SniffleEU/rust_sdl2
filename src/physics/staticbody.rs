use super::aabb::Aabb;
use sdl2::rect::Rect;

pub enum BodyType{
    Solid,
    Death,
}

pub struct StaticBody {
    aabb: Aabb,
    body_type: BodyType,
}

impl StaticBody {
    pub fn new() -> StaticBody {
        StaticBody {
            aabb: Aabb::new(&Rect::new(0, 0, 10, 10)),
            body_type: BodyType::Solid,
        }
    }

}