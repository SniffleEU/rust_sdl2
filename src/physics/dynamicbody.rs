use super::aabb::Aabb;
use crate::utils::timer::Timer;
use sdl2::rect::Rect;
use vector2d::Vector2D;

pub struct DynamicBody {
    pub speed: Vector2D<f64>,
    old_speed: Vector2D<f64>,

    pub position: Vector2D<f64>,
    old_position: Vector2D<f64>,

    pub hitbox: Aabb,
    aabb_offset: Vector2D<f64>,

    pushed_right_wall: bool,
    pub pushes_right_wall: bool,

    pushed_left_wall: bool,
    pub pushes_left_wall: bool,

    pub on_ground: bool,
    was_on_ground: bool,

    pub at_ceiling: bool,
    was_at_ceiling: bool,
    // scale: Vector2D<f64>,
}

impl DynamicBody {
    pub fn new() -> DynamicBody {
        DynamicBody {
            speed: Vector2D::new(0., 0.),
            old_speed: Vector2D::new(0., 0.),
            position: Vector2D::new(0., 0.),
            old_position: Vector2D::new(0., 0.),
            hitbox: Aabb::new(&Rect::new(0, 0, 4, 4)),
            aabb_offset: Vector2D::new(1., 1.),
            pushed_right_wall: false,
            pushes_right_wall: false,
            pushed_left_wall: false,
            pushes_left_wall: false,
            on_ground: false,
            was_on_ground: false,
            at_ceiling: false,
            was_at_ceiling: false,
            // scale: Vector2D::new(0., 0.),
        }
    }

    pub fn update_physics(&mut self, delta_time: &Timer) {
        self.old_position = self.position;
        self.old_speed = self.speed;

        self.pushed_right_wall = self.pushes_right_wall;
        self.pushed_left_wall = self.pushes_left_wall;

        self.was_on_ground = self.on_ground;
        self.was_at_ceiling = self.at_ceiling;

        self.position =
            self.position + (self.speed * (delta_time.get_ticks().as_millis() as f64 / 1000.));

        if self.position.y < 0. {
            self.position.y = 0.;
            self.on_ground = true;
        } else {
            self.on_ground = false;
        }

        self.hitbox.center = self.position + self.aabb_offset;

        self.position.x = self.position.x.ceil();
        self.position.y = self.position.y.ceil();
    }

    pub fn is_ground(&self, other: &Rect) -> bool{
        //Rect hitbox under the real hitbox to check if there is collision with something under it
        let testrect = Aabb::new(&Rect::new(
            (self.hitbox.center.x - self.hitbox.half_size.x + self.aabb_offset.x) as i32, 
            (self.hitbox.center.y + self.hitbox.half_size.y + self.aabb_offset.y) as i32, 
            (self.hitbox.center.x + self.hitbox.half_size.x - self.aabb_offset.x) as u32, 
            (self.hitbox.center.y + self.hitbox.half_size.y + (self.aabb_offset.y * 2.)) as u32));
        let otheraabb = Aabb::new(other);
        testrect.intersect(otheraabb)
    }

    pub fn is_wall(&self, other: &Rect) -> bool {
        //axis box for left side of the rectangle
        let test_rect = Aabb::new(&Rect::new(
            (self.hitbox.center.x - self.hitbox.half_size.x - (self.aabb_offset.x * 2.)) as i32, 
            (self.hitbox.center.y - self.hitbox.half_size.y - self.aabb_offset.y) as i32, 
            (self.hitbox.center.x - self.hitbox.half_size.x - self.aabb_offset.x) as u32, 
            (self.hitbox.center.y + self.hitbox.half_size.y - self.aabb_offset.y) as u32));
        
        //temporary for testing if wall is on the right
        let test_rect2 = Aabb::new(&Rect::new(
            (self.hitbox.center.x + self.hitbox.half_size.x + self.aabb_offset.x) as i32, 
            (self.hitbox.center.y - self.hitbox.half_size.y - self.aabb_offset.y) as i32, 
            (self.hitbox.center.x + self.hitbox.half_size.x + (self.aabb_offset.x * 2.)) as u32, 
            (self.hitbox.center.y + self.hitbox.half_size.y - self.aabb_offset.y) as u32));

        let otheraabb = Aabb::new(other);
        test_rect.intersect(otheraabb)
    }
}
