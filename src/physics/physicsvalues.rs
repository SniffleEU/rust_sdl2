pub const GRAVITY: f64 = -10.;
pub const MAX_FALLING_SPEED: f64 = 400.;
pub const MIN_JUMPING_SPEED: f64 = 200.;