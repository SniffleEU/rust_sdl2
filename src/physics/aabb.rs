use sdl2::rect::Rect;
use vector2d::Vector2D;

pub struct Aabb {
    pub center: Vector2D<f64>,
    pub half_size: Vector2D<f64>,
}

impl Aabb {
    pub fn new(rect: &Rect) -> Aabb {
        Aabb {
            center: Vector2D {
                x: rect.center().x() as f64,
                y: rect.center().y() as f64,
            },
            half_size: Vector2D {
                x: (rect.width() / 2) as f64,
                y: (rect.height() / 2) as f64,
            },
        }
    }

    pub fn intersect(&self, other: Aabb) -> bool {
        if (self.center.x - other.center.x).abs() > self.half_size.x + other.half_size.x {
            return false;
        }
        if (self.center.y - other.center.y).abs() > self.half_size.y + other.half_size.y {
            return false;
        }
        true
    }
}
