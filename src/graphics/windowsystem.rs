use sdl2::image::{self, InitFlag, Sdl2ImageContext};
use sdl2::render::WindowCanvas;
use sdl2::*;

pub const WIDTH: u32 = 640;
pub const HEIGHT: u32 = 480;

pub struct WindowManager {
    pub sdl2_context: Sdl,
    _sdl2_image_context: Sdl2ImageContext,
    pub canvas: WindowCanvas,
    pub texture_creator: render::TextureCreator<sdl2::video::WindowContext>,
}

impl WindowManager {
    pub fn new() -> WindowManager {
        let init_sdl2 = sdl2::init().expect("Failed to init SDL");
        let init_sdl2_image =
            image::init(InitFlag::PNG | InitFlag::JPG).expect("Failed to load sdl_image");
        let init_canvas = {
            let video_subsystem = init_sdl2.video().expect("Failed to get video context");

            let window = video_subsystem
                .window("Rust-sdl2 testing", WIDTH, HEIGHT)
                .position_centered()
                .build()
                .expect("Failed to build window");

            window
                .into_canvas()
                //.present_vsync()
                .build()
                .expect("Failed to build window's canvas")
        };
        let init_texture_creator = init_canvas.texture_creator();

        WindowManager {
            sdl2_context: init_sdl2,
            _sdl2_image_context: init_sdl2_image,
            canvas: init_canvas,
            texture_creator: init_texture_creator,
        }
    }
}
