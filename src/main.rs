#![allow(dead_code)]
use sdl2::event::Event;
use sdl2::image::LoadTexture;
use sdl2::keyboard::Keycode;
use sdl2::rect::{Point, Rect};

use std::time::Duration;

mod enteties;
use enteties::player::{Direction, Player};
mod graphics;
use graphics::windowsystem::WindowManager;
mod utils;
use utils::timer;
mod physics;

fn main() {
    let mut window_manager: WindowManager = graphics::windowsystem::WindowManager::new();

    let mut player = Player {
        sprite: Rect::new(0, 0, 26, 36),
        direction: Direction::Right,
        current_frame: 0,
        texture: window_manager
            .texture_creator
            .load_texture("res/bardo.png")
            .expect("failed to load texture"),
        velocity_x: 0,
        velocity_y: 0,
        position_box: Rect::from_center(Point::new(50, 50), 26, 36),
    };

    let mut event_pump = window_manager
        .sdl2_context
        .event_pump()
        .expect("Failed to get event pump");

    //Used to calculate how long to sleep on each frame
    let mut frame_timer = timer::Timer::new();

    let mut world = enteties::world::World::test_level_from_tmx(&window_manager.texture_creator);

    window_manager
        .canvas
        .set_logical_size(100, 100)
        .expect("logic size lmao");
    let (screen_width, screen_height) = window_manager.canvas.logical_size();

    let mut camera = Rect::new(0, 0, screen_width, screen_height);

    //Timer for delta time calculation
    let mut timestep = timer::Timer::new();

    'running: loop {
        frame_timer.restart();

        window_manager.canvas.clear();

        for event in event_pump.poll_iter() {
            match event {
                Event::Quit { .. }
                | Event::KeyDown {
                    keycode: Some(Keycode::Escape),
                    ..
                } => break 'running,
                _ => {}
            }
            player.update_player(&event);
        }
        player.animate();
        player.move_player(&world.levels[0].level_hitboxes, &timestep);
        timestep.restart();

        camera.set_x(
            (player.position_box.x() + (player.sprite.width() as i32 / 2))
                - (screen_width as i32 / 2),
        );
        camera.set_y(
            (player.position_box.y() + (player.sprite.height() as i32 / 2))
                - (screen_height as i32 / 2),
        );

        //TODO: Move to its own module
        if camera.x < 0 {
            camera.x = 0;
        }
        if camera.y < 0 {
            camera.y = 0;
        }
        if camera.x >= (world.level_width as i32 - camera.w) {
            camera.x = world.level_width as i32 - camera.w;
        }
        if camera.y >= (world.level_heigth as i32 - camera.h) {
            camera.y = world.level_heigth as i32 - camera.h;
        }

        window_manager
            .canvas
            .copy(&world.levels[0].level_tile_map, camera, None)
            .expect("failed to use background image");

        //world.levels[0].test_draw_level(&mut window_manager.canvas, &camera);
        world.levels[0].draw_tmx_map(&mut window_manager.canvas, &camera);
        player.render(&mut window_manager.canvas, &camera);

        window_manager.canvas.present();

        ::std::thread::sleep(Duration::new(
            0,
            (1_000_000_000u32 / 144)
                - num::clamp(
                    frame_timer.get_ticks().as_nanos() as u32,
                    0,
                    1_000_000_000u32 / 144,
                ),
        ));
    }
}
