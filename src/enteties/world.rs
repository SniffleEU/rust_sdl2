use super::level::Level;
use sdl2::render::Texture;

pub struct World<'r> {
    pub level_width: i32,
    pub level_heigth: i32,
    pub levels: Vec<Level<'r>>,
}

impl<'r> World<'r> {
    pub fn test_new(level_texture: Texture<'r>) -> World<'r> {
        World {
            level_heigth: 960,
            level_width: 1280,
            levels: vec![Level::test_level(level_texture)],
        }
    }

    pub fn test_level_from_tmx(
        texture_creator: &'r sdl2::render::TextureCreator<sdl2::video::WindowContext>,
    ) -> World<'r> {
        World {
            level_heigth: 800,
            level_width: 800,
            levels: vec![Level::new_from_tmx(&texture_creator)],
        }
    }
}
