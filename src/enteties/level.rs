use sdl2::image::LoadTexture;
use sdl2::rect::Rect;
use sdl2::render::Texture;
use sdl2::render::WindowCanvas;
use tiled::*;

#[derive(Debug)]
pub struct MapTile {
    flip_h: bool,
    flip_v: bool,
    flip_d: bool,
    texture: u8,
    position: Rect,
}

pub struct Level<'r> {
    pub level_hitboxes: Vec<Rect>,
    pub level_tile_map: Texture<'r>,
    pub level_tiles: Vec<MapTile>,
    pub level_tilemap_render_locations: Vec<Rect>,
}

impl<'r> Level<'r> {
    pub fn test_level(level_texture: Texture) -> Level {
        Level {
            level_hitboxes: vec![
                Rect::new(50, 50, 50, 50),
                Rect::new(150, 150, 50, 50),
                Rect::new(250, 250, 50, 50),
                Rect::new(350, 350, 50, 50),
                Rect::new(450, 450, 50, 50),
            ],
            level_tile_map: level_texture,
            level_tiles: vec![MapTile {
                flip_h: false,
                flip_d: false,
                flip_v: false,
                texture: 0,
                position: Rect::new(0, 0, 0, 0),
            }],
            level_tilemap_render_locations: vec![Rect::new(0, 0, 1, 1)],
        }
    }

    pub fn test_draw_level(&mut self, canvas: &mut WindowCanvas, camera: &Rect) {
        canvas.set_draw_color(sdl2::pixels::Color::RGB(255, 255, 255));

        for hitbox in self.level_hitboxes.iter() {
            let draw_box = Rect::new(
                hitbox.x() - camera.x(),
                hitbox.y() - camera.y(),
                hitbox.width(),
                hitbox.height(),
            );

            canvas
                .draw_rect(draw_box)
                .expect("Failed at drawing test map");
            canvas
                .fill_rect(draw_box)
                .expect("Failed at colouring test map");
        }

        canvas.set_draw_color(sdl2::pixels::Color::RGB(0, 0, 0));
    }

    //Add string for loading the correct level with path to tmx
    pub fn new_from_tmx(
        window_stuff: &sdl2::render::TextureCreator<sdl2::video::WindowContext>,
    ) -> Level {
        let file = std::fs::File::open("res/caves_map.tmx").expect("Failed to open tmx file");
        let reader = std::io::BufReader::new(file);
        let map = parse(reader).expect("Failed to parse tmx file");

        let mut the_tiles: Vec<MapTile> = Vec::with_capacity((map.width * map.height) as usize);
        let mut the_hitboxes: Vec<Rect> = Vec::new();

        let (mut x, mut y, tile_width, tile_height) = (0, 0, map.tile_width, map.tile_height);

        match &map.layers[0].tiles {
            tiled::LayerData::Finite(v) => {
                for tiles in v {
                    for tile in tiles {
                        the_tiles.push(MapTile {
                            flip_d: tile.flip_d,
                            flip_h: tile.flip_h,
                            flip_v: tile.flip_v,
                            texture: tile.gid as u8,
                            position: Rect::new(x, y, tile_width, tile_height),
                        });
                        x = x + tile_width as i32;
                    }
                    x = 0;
                    y = y + tile_height as i32;
                }
            }
            _ => {}
        }

        for object_group in map.object_groups.iter() {
            if object_group.name == "Collision layer" {
                for object in object_group.objects.iter() {
                    the_hitboxes.push(Rect::new(
                        object.x as i32,
                        object.y as i32,
                        object.width as u32,
                        object.height as u32,
                    ))
                }
            }
        }

        let tileset_for_level: Texture;

        let mut path_to_tileset = String::from("res/");
        path_to_tileset = path_to_tileset + &map.tilesets[0].images[0].source;

        tileset_for_level = window_stuff
            .load_texture(path_to_tileset)
            .expect("could not load tilemap");

        let mut render_locations: Vec<Rect> = vec![];

        let (mut x_pos, mut y_pos) = (0, 0);

        for _n in 0..(map.tilesets[0].images[0].height / map.tilesets[0].tile_height as i32) {
            for _j in 0..(map.tilesets[0].images[0].width / map.tilesets[0].tile_width as i32) {
                render_locations.push(Rect::new(
                    x_pos,
                    y_pos,
                    map.tilesets[0].tile_width,
                    map.tilesets[0].tile_height,
                ));
                x_pos = x_pos + map.tilesets[0].tile_width as i32;
            }
            x_pos = 0;
            y_pos = y_pos + map.tilesets[0].tile_height as i32;
        }

        Level {
            level_hitboxes: the_hitboxes,
            level_tile_map: tileset_for_level,
            level_tiles: the_tiles,
            level_tilemap_render_locations: render_locations,
        }

        //println!("Tiles vector test {:?}", the_tiles);

        //println!("What the fields are {:?}", the_hitboxes);
    }

    pub fn draw_tmx_map(&mut self, canvas: &mut WindowCanvas, camera: &Rect) {
        for tile in self.level_tiles.iter() {
            let draw_box = Rect::new(
                tile.position.x() - camera.x(),
                tile.position.y() - camera.y(),
                tile.position.width(),
                tile.position.height(),
            );
            //Usize -1 because vector starts at 0, but tiled map manager starts at 1
            canvas
                .copy_ex(
                    &self.level_tile_map,
                    self.level_tilemap_render_locations[tile.texture as usize - 1],
                    draw_box,
                    0.,
                    None,
                    tile.flip_h,
                    tile.flip_v,
                )
                .expect("Failed to draw tile");
        }
    }
}
