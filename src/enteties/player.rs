use crate::utils::timer::Timer;
use sdl2::event::Event;
use sdl2::keyboard::Keycode;
use sdl2::rect::Rect;
use sdl2::render::{Texture, WindowCanvas};

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Direction {
    Up = 3,
    Down = 0,
    Left = 1,
    Right = 2,
}
const PLAYER_SPEED: i32 = 144;

pub struct Player<'r> {
    pub sprite: Rect,
    pub direction: Direction,
    pub current_frame: i32,
    pub texture: Texture<'r>,
    pub velocity_x: i32,
    pub velocity_y: i32,
    pub position_box: Rect,
}

impl<'r> Player<'r> {
    pub fn update_player(&mut self, event: &Event) {
        match event {
            Event::KeyDown {
                keycode: Some(Keycode::Left),
                repeat: false,
                ..
            } => {
                self.velocity_x = self.velocity_x - PLAYER_SPEED;
            }
            Event::KeyDown {
                keycode: Some(Keycode::Right),
                repeat: false,
                ..
            } => {
                self.velocity_x = self.velocity_x + PLAYER_SPEED;
            }
            Event::KeyDown {
                keycode: Some(Keycode::Down),
                repeat: false,
                ..
            } => {
                self.velocity_y = self.velocity_y + PLAYER_SPEED;
            }
            Event::KeyDown {
                keycode: Some(Keycode::Up),
                repeat: false,
                ..
            } => {
                self.velocity_y = self.velocity_y - PLAYER_SPEED;
            }
            Event::KeyUp {
                keycode: Some(Keycode::Right),
                repeat: false,
                ..
            } => {
                self.velocity_x = self.velocity_x - PLAYER_SPEED;
            }
            Event::KeyUp {
                keycode: Some(Keycode::Left),
                repeat: false,
                ..
            } => {
                self.velocity_x = self.velocity_x + PLAYER_SPEED;
            }
            Event::KeyUp {
                keycode: Some(Keycode::Up),
                repeat: false,
                ..
            } => {
                self.velocity_y = self.velocity_y + PLAYER_SPEED;
            }
            Event::KeyUp {
                keycode: Some(Keycode::Down),
                repeat: false,
                ..
            } => {
                self.velocity_y = self.velocity_y - PLAYER_SPEED;
            }
            _ => {}
        }
    }

    pub fn move_player(&mut self, floor: &Vec<Rect>, delta_time: &Timer) {
        self.position_box.set_x(
            self.position_box.x
                + (self.velocity_x as f64 * (delta_time.get_ticks().as_millis() as f64 / 1000.))
                    as i32,
        );

        //Sometimes it backs off 3 pixels others 4! maybe clamp value on collision?
        if self.collide_vec(&floor) {
            self.position_box.set_x(
                self.position_box.x
                    - (self.velocity_x as f64 * (delta_time.get_ticks().as_millis() as f64 / 1000.))
                        as i32,
            );
        }

        self.position_box.set_y(
            self.position_box.y
                + (self.velocity_y as f64 * (delta_time.get_ticks().as_millis() as f64 / 1000.))
                    as i32,
        );

        //Sometimes it backs off 3 pixels others 4! maybe clamp value on collision?
        if self.collide_vec(&floor) {
            self.position_box.set_y(
                self.position_box.y
                    - (self.velocity_y as f64 * (delta_time.get_ticks().as_millis() as f64 / 1000.))
                        as i32,
            );
        }
    }

    pub fn animate(&mut self) {
        if self.velocity_y > 0 {
            self.direction = Direction::Down;
        } else if self.velocity_y < 0 {
            self.direction = Direction::Up;
        }

        if self.velocity_x > 0 {
            self.direction = Direction::Right;
        } else if self.velocity_x < 0 {
            self.direction = Direction::Left;
        }

        if self.velocity_x != 0 || self.velocity_y != 0 {
            self.current_frame = (self.current_frame + 1) % 3;
        }
    }

    pub fn collide_single(&self, other_rect: &Rect) -> bool {
        if (self.position_box.y + self.position_box.height() as i32) <= other_rect.y {
            return false;
        }

        if self.position_box.y >= other_rect.y + other_rect.height() as i32 {
            return false;
        }

        if self.position_box.x + self.position_box.width() as i32 <= other_rect.x() {
            return false;
        }

        if self.position_box.x >= other_rect.x + other_rect.width() as i32 {
            return false;
        }

        true
    }

    pub fn collide_vec(&self, level_boxes: &Vec<Rect>) -> bool {
        for hitbox in level_boxes.iter() {
            if (((self.position_box.y + self.position_box.height() as i32) <= hitbox.y)
                | (self.position_box.y >= hitbox.y + hitbox.height() as i32)
                | (self.position_box.x + self.position_box.width() as i32 <= hitbox.x())
                | (self.position_box.x >= hitbox.x + hitbox.width() as i32))
                == false
            {
                return true;
            }
        }
        false
    }

    pub fn render(&mut self, canvas: &mut WindowCanvas, camera: &Rect) {
        let current_frame = Rect::new(
            //Next line was for animation testing, made it update 3 times
            //frame_width as i32 * ((animation_timer.get_ticks().as_secs() as i32 % 3)) as i32,
            self.sprite.x() + self.sprite.width() as i32 * self.current_frame,
            self.sprite.y() + self.sprite.height() as i32 * self.direction as i32,
            self.sprite.width(),
            self.sprite.height(),
        );

        let draw_position = Rect::new(
            self.position_box.x() - camera.x(),
            self.position_box.y() - camera.y(),
            self.sprite.width(),
            self.sprite.height(),
        );

        canvas
            .copy(&self.texture, current_frame, draw_position)
            .expect("render player");
    }
}
