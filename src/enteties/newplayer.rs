use crate::physics::dynamicbody::DynamicBody;
use crate::physics::physicsvalues::*;
use crate::utils::timer::Timer;
use sdl2::render::WindowCanvas;
use sdl2::event::Event;
use sdl2::keyboard::Keycode;

#[derive(Copy, Clone)]
enum KeyInput {
    GoLeft,
    GoRight,
    GoDown,
    Jump,
    Count,
}

#[derive(Copy, Clone)]
enum PlayerState {
    Stand,
    Walk,
    Jump,
    GrabLedge,
}

pub struct NewPlayer {
    state: PlayerState,
    jump_speed: f64,
    walk_speed: f64,
    dynamic_body: DynamicBody,
    inputs: [bool; KeyInput::Count as usize],
    last_input: [bool; KeyInput::Count as usize],
}

impl NewPlayer {
    pub fn new() -> NewPlayer {
        NewPlayer {
            state: PlayerState::Stand,
            jump_speed: 410.,
            walk_speed: 160.,
            dynamic_body: DynamicBody::new(),
            inputs: [false; KeyInput::Count as usize],
            last_input: [false; KeyInput::Count as usize],
        }
    }

    pub fn update_inputs(&mut self, event: &Event){
        match event {
            Event::KeyDown {
                keycode: Some(Keycode::Left),
                ..
            } => {
                self.inputs[KeyInput::GoLeft as usize] = true;
            }
            Event::KeyDown {
                keycode: Some(Keycode::Right),
                ..
            } => {
                self.inputs[KeyInput::GoRight as usize] = true;
            }
            Event::KeyDown {
                keycode: Some(Keycode::Down),
                ..
            } => {
                self.inputs[KeyInput::GoDown as usize] = true;
            }
            Event::KeyDown {
                keycode: Some(Keycode::Up),
                ..
            } => {
                self.inputs[KeyInput::Jump as usize] = true;
            }
            Event::KeyUp {
                keycode: Some(Keycode::Right),
                ..
            } => {
                self.inputs[KeyInput::GoRight as usize] = false;
            }
            Event::KeyUp {
                keycode: Some(Keycode::Left),
                ..
            } => {
                self.inputs[KeyInput::GoLeft as usize] = false;
            }
            Event::KeyUp {
                keycode: Some(Keycode::Up),
                ..
            } => {
                self.inputs[KeyInput::Jump as usize] = false;
            }
            Event::KeyUp {
                keycode: Some(Keycode::Down),
                ..
            } => {
                self.inputs[KeyInput::GoDown as usize] = false;
            }
            _ => {}
        }
    }

    pub fn update_player_state(&mut self, delta_time: &Timer) {
        //animate based on state later
        match self.state {
            PlayerState::Stand => {
                self.dynamic_body.speed.x = 0.;
                self.dynamic_body.speed.y = 0.;

                if !self.dynamic_body.on_ground {
                    self.state = PlayerState::Jump;
                } else if self.key_state(KeyInput::GoRight) != self.key_state(KeyInput::GoLeft) {
                    self.state = PlayerState::Walk;
                } else if self.key_state(KeyInput::Jump) {
                    self.dynamic_body.speed.y = self.jump_speed;
                    self.state = PlayerState::Jump;
                }
            }
            PlayerState::Walk => {
                if self.key_state(KeyInput::GoRight) == self.key_state(KeyInput::GoLeft) {
                    self.state = PlayerState::Stand;
                    self.dynamic_body.speed.x = 0.;
                    self.dynamic_body.speed.y = 0.;
                } else {
                    if self.key_state(KeyInput::GoRight) {
                        if self.dynamic_body.pushes_right_wall {
                            self.dynamic_body.speed.x = 0.;
                        } else {
                            self.dynamic_body.speed.x = self.walk_speed;
                        }
                    } else if self.key_state(KeyInput::GoLeft) {
                        if self.dynamic_body.pushes_left_wall {
                            self.dynamic_body.speed.x = 0.;
                        } else {
                            self.dynamic_body.speed.x = -self.walk_speed;
                        }
                    }
                    if self.key_state(KeyInput::Jump) {
                        self.dynamic_body.speed.y = self.jump_speed;
                        self.state = PlayerState::Jump;
                    } else if !self.dynamic_body.on_ground {
                        self.state = PlayerState::Jump;
                    }
                }
            }
            PlayerState::Jump => {
                //TODO: ADJUST GRAVITY N STUFF
                //Pretty sure any issues will come from here:
                self.dynamic_body.speed.y = self.dynamic_body.speed.y + (GRAVITY * (delta_time.get_ticks().as_millis() as f64 / 1000.)); 
                self.dynamic_body.speed.y = self.dynamic_body.speed.y.max(MAX_FALLING_SPEED);
                //TODO: to here.

                if self.key_state(KeyInput::GoRight) == self.key_state(KeyInput::GoLeft) {
                    self.dynamic_body.speed.x = 0.;
                } else {
                    if self.key_state(KeyInput::GoRight) {
                        if self.dynamic_body.pushes_right_wall {
                            self.dynamic_body.speed.x = 0.;
                        } else {
                            self.dynamic_body.speed.x = self.walk_speed;
                        }
                    } else if self.key_state(KeyInput::GoLeft) {
                        if self.dynamic_body.pushes_left_wall {
                            self.dynamic_body.speed.x = 0.;
                        } else {
                            self.dynamic_body.speed.x = -self.walk_speed;
                        }
                    }
                }
                if !self.key_state(KeyInput::Jump) && self.dynamic_body.speed.y > 0. {
                    //This is for having a minimum jumpheight
                    //I think
                    //Will probably have issues here
                    //If jump is pressed and goes into jump state, but jump button is released this happens
                    //Then takes the lowest value of y current speed and min jumping speed
                    //If jump is released after speed is lower than min speed then nothing happens
                    self.dynamic_body.speed.y.min(MIN_JUMPING_SPEED);
                }

                if self.dynamic_body.on_ground {
                    if self.key_state(KeyInput::GoRight) == self.key_state(KeyInput::GoLeft) {
                        self.state = PlayerState::Stand;
                        self.dynamic_body.speed.x = 0.;
                        self.dynamic_body.speed.y = 0.;
                    }else {
                        self.state = PlayerState::Walk;
                        self.dynamic_body.speed.y = 0.;
                    }
                }
            }
            PlayerState::GrabLedge => {}
        }
        self.dynamic_body.update_physics(delta_time);
        self.update_last_inputs();
    }

    fn released_key(&self, key: KeyInput) -> bool {
        !self.inputs[key as usize] && self.last_input[key as usize]
    }

    fn key_state(&self, key: KeyInput) -> bool {
        self.inputs[key as usize]
    }

    fn pressed_key(&self, key: KeyInput) -> bool {
        self.inputs[key as usize] && !self.last_input[key as usize]
    }

    fn update_last_inputs(&mut self) {
        for i in 0..self.inputs.len() {
            self.last_input[i] = self.inputs[i];
        }
    }

    pub fn render(&self, canvas: &mut WindowCanvas) {
       let something = canvas.window().size();
       println!("{:?}", something); 
    }

}
